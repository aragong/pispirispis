const pispirispisApp = angular.module('pispirispis', []);

pispirispisApp.controller('main', [
    '$scope',
    '$q',
    function($scope, $q){
        let defered = $q.defer();
        let promise = defered.promise;

        $scope.tiempoTranscurrido = 0;

        promise.then(function(resultado) {
        }, function(error) {
        }, function() {
            $scope.tiempoTranscurrido++;

            if(($scope.tiempoTranscurrido % 2) === 1)
                $scope.crearPoblacionInicial();

            $scope.gestionarAlicanolas();
            $scope.recorrerPoblacion();

            if($scope.tropus.individuos.length === 0 && $scope.inopios.individuos.length === 0)
                $scope.avanzar = false;

            //GRAFICAR
            if(($scope.tiempoTranscurrido % 30) === 0) {
                $scope.chartAnios.push($scope.tiempoTranscurrido);

                $scope.chartTotalTropus.push($scope.tropus.individuos.length);
                $scope.chartTotalInopios.push($scope.inopios.individuos.length);

                $scope.chartTropusMachos.push($scope.tropus.machos);
                $scope.chartTropusHembras.push($scope.tropus.hembras);

                $scope.chartInopiosMachos.push($scope.inopios.machos);
                $scope.chartInopiosHembras.push($scope.inopios.hembras);

                $scope.chartTotalAlicanolas.push($scope.plano.alicanolas.length);
                $scope.chartTotalFifirufas.push($scope.plano.fifirufas.length);

                $scope.graficar();
            }
        });


        $scope.avanzar = false;

        $scope.etapasConfig = [
            new Etapa('Nacimiento', 0, 0),
            new Etapa('Infancia', (75*2), 1),
            new Etapa('Adolecencia', (75*3), 2),
            new Etapa('Adultez', (75*40), 3),
            new Etapa('Vejez', (75*3), 4),
            new Etapa('Muerte', 0, 0)
        ];

        $scope.planoConf = {
            ancho: 1040,
            alto: 650
        };
        $scope.generos = pispirispiConf.generos;
        $scope.poblacionInicial = {
            tropus: { machos: 30, hembras: 30 },
            inopios: { machos: 30, hembras: 30 }
        };

        $scope.alicanolasInicial = 30;
        $scope.generacionAlicanolas = {
            tiempo: 10,
            cantidadMax: 3
        };

        $scope.energiaFifirufa = 100;

        $scope.velocidad = 50;
        $scope.calcularVelocidad = function(){
            return timepoMax / $scope.velocidad;
        };

        $scope.mostrarDatos = false;


        $scope.crearPoblacionInicial = function(){
            const tipo = parseInt(random(0, 2));
            const genero = parseInt(random(0, 2));
            let poblacion = $scope.tropus;
            let poblacionConf = $scope.tmpPoblacionConf.tropus;
            if(tipo === 1){
                poblacion = $scope.inopios;
                poblacionConf = $scope.tmpPoblacionConf.inopios;
            }
            if((poblacionConf.machos + poblacionConf.hembras) > 0){
                let generoObj = null;
                if(genero === 1 && poblacionConf.machos > 0){
                    generoObj = pispirispiConf.generos.m;
                    poblacionConf.machos--;
                }
                else if(genero === 0 && poblacionConf.hembras > 0){
                    generoObj = pispirispiConf.generos.h;
                    poblacionConf.hembras--;
                }
                if(generoObj !== null) {
                    poblacion.agregarIndividuo(
                        generoObj,
                        0,
                        funcionalidades.posicionAzar($scope.plano.getDimensiones()));
                }
            }
        };

        $scope.recorrerPoblacion = function(){
            let pispirispisLista = [];

            function verificarColicion(pispirispi, poblacion){
                let poblacion1 = poblacion;
                let poblacion2 = (poblacion === $scope.tropus)?$scope.inopios:$scope.tropus;

                $.each($scope.plano.alicanolas, function(index, alicanola){
                    if(pispirispi.colision(alicanola.posicion, alicanola.tamanio)){
                        if(alicanola.tamanio <= (pispirispi.tamanio / 1.5)){
                            pispirispi.energia += alicanola.tamanio*0.75*100;
                            pispirispi.alistarFifirufa(alicanola.tamanio*0.25);
                            $scope.plano.eliminarAlicanola(alicanola);
                        }
                    }
                });

                $.each($scope.plano.fifirufas, function(index, fifirufa){
                    if(pispirispi.colision(fifirufa.posicion, fifirufa.tamanio)){
                        if(!(pispirispi.tipo === 'inopios' && pispirispi.etapa.etapa === 3))
                            pispirispi.energia = pispirispi.energia - $scope.energiaFifirufa;
                        else
                            $scope.plano.eliminarFifirufa(fifirufa);
                    }
                });

                $.each(pispirispisLista, function(index, value){
                    if(pispirispi.colision(value.getPosicion(), value.tamanio)){
                        if(pispirispi.tipo !== value.tipo){
                            if(pispirispi.genero === value.genero &&
                                (pispirispi.etapa.etapa === 2 && value.etapa.etapa === 2)){
                                if(pispirispi.energia < value.energia){
                                    value.etapa.evolucionar = true;
                                    poblacion1.matarIndividuo(pispirispi);
                                }
                                else if(pispirispi.energia > value.energia){
                                    pispirispi.etapa.evolucionar = true;
                                    poblacion2.matarIndividuo(value);
                                }
                            }
                            else if(pispirispi.edad < value.edad){
                                value.etapa.evolucionar = true;
                                poblacion1.matarIndividuo(pispirispi);
                            }
                            else if(pispirispi.edad > value.edad){
                                pispirispi.etapa.evolucionar = true;
                                poblacion2.matarIndividuo(value);
                            }
                        }
                        else{
                            if(pispirispi.genero !== value.genero){
                                if(pispirispi.etapa.etapa === 3 && value.etapa.etapa === 3){ //REPRODUCCION
                                    pispirispi.reproduccion = true;
                                }
                            }
                        }


                        pispirispi.setDireccion();
                        value.setDireccion();
                    }
                });
                pispirispisLista.push(pispirispi);
            }
            function necesitaDefecar(pispirispi){
                if(pispirispi.tiempoDefecar()){
                    $scope.plano.crearFifirufa(new Punto(pispirispi.getPosicion().x, pispirispi.getPosicion().y), pispirispi.fifirufa.tamanio);
                    pispirispi.defecar();
                }
            }
            function recorrerPoblacion(poblacion){
                $.each(poblacion.individuos, function(index, pispirispi){
                    pispirispi.aumentarEdad();
                    pispirispi.movimiento($scope.plano);

                    if(pispirispi.etapa.etapa === (pispirispi.etapas.length - 1) || pispirispi.energia <= 0)
                        poblacion.matarIndividuo(pispirispi);
                    else{
                        verificarColicion(pispirispi, poblacion);
                        necesitaDefecar(pispirispi);
                    }

                    if(pispirispi.reproduccion){
                        let colision = false;
                        $.each(poblacion.individuos, function(key, pareja){
                            if(pispirispi !== pareja){
                                if(pispirispi.colision(pareja.getPosicion(), pareja.tamanio)){
                                    colision = true;
                                }
                            }
                        });
                        if(!colision){
                            pispirispi.reproduccion = false;
                            let generoObj = (parseInt(random(0, 2)) === 1)?pispirispiConf.generos.m:pispirispiConf.generos.h;
                            poblacion.agregarIndividuo(
                                generoObj,
                                0,
                                new Punto(pispirispi.getPosicion().x, pispirispi.getPosicion().y));
                        }
                    }
                });
            }

            recorrerPoblacion($scope.tropus);
            recorrerPoblacion($scope.inopios);
        };
        $scope.gestionarAlicanolas = function(){
            //CREAR
            if(($scope.tiempoTranscurrido % $scope.generacionAlicanolas.tiempo) === 0){
                for(let i = 0; i < parseInt(random(-1, $scope.generacionAlicanolas.cantidadMax + 1)); i++){
                    $scope.plano.crearAlicanola();
                }
            }
            //AUMENTAR TAMAÑO
            $scope.plano.crecerAlicanolas();
        };

        //INIT
        $scope.iniciar = async function(){
            $scope.avanzar = true;

            $scope.plano = new Plano($scope.planoConf.ancho, $scope.planoConf.alto);

            $scope.tropus = new Poblacion('tropus',
                0,
                0,
                $scope.etapasConfig,
                $scope.plano.getDimensiones()
            );
            $scope.inopios = new Poblacion('inopios',
                0,
                0,
                $scope.etapasConfig,
                $scope.plano.getDimensiones()
            );
            $scope.tmpPoblacionConf = duplicateObject($scope.poblacionInicial);

            for(let i = 0; i < $scope.alicanolasInicial; i++){
                $scope.plano.crearAlicanola();
            }

            while($scope.avanzar){
                await sleep($scope.calcularVelocidad());
                defered.notify();
            }
        };

        $scope.submit = function(){
            if(!$scope.avanzar)
                $scope.iniciar();
            else
                $scope.avanzar = false;
        };


        $scope.toogleDatos = function(){
            $scope.mostrarDatos = !$scope.mostrarDatos;
        };

        // GRAFICAR DATOS
        $scope.chartAnios = []; //xAxis

        $scope.chartPoblacion = {
            chart: new MyChart('canvas-1','Población Total'),
            datasets: [
                new ChartDataSet('Tropus', MyChart.chartColors().tropus),
                new ChartDataSet('Inopios', MyChart.chartColors().inopius)
            ]
        };
        $scope.chartTotalTropus = [];
        $scope.chartTotalInopios = [];

        $scope.chartTropus = {
            chart: new MyChart('canvas-2','Población de Tropus'),
            datasets: [
                new ChartDataSet('Machos', MyChart.chartColors().blue),
                new ChartDataSet('Hembras', MyChart.chartColors().red)
            ]
        };
        $scope.chartTropusMachos = [];
        $scope.chartTropusHembras = [];

        $scope.chartInopios = {
            chart: new MyChart('canvas-3','Población de Inopios'),
            datasets: [
                new ChartDataSet('Machos', MyChart.chartColors().blue),
                new ChartDataSet('Hembras', MyChart.chartColors().red)
            ]
        };
        $scope.chartInopiosMachos = [];
        $scope.chartInopiosHembras = [];

        $scope.chartAliFifi = {
            chart: new MyChart('canvas-4','Alicanolas - Fifirufas'),
            datasets: [
                new ChartDataSet('Alicanolas', MyChart.chartColors().purple),
                new ChartDataSet('Fifirufas', MyChart.chartColors().orange)
            ]
        };
        $scope.chartTotalAlicanolas = [];
        $scope.chartTotalFifirufas = [];

        $scope.graficar = function(){
            $scope.chartPoblacion.datasets[0].addDataAll($scope.chartTotalTropus);
            $scope.chartPoblacion.datasets[1].addDataAll($scope.chartTotalInopios);
            $scope.chartPoblacion.chart.addlabels($scope.chartAnios);
            $scope.chartPoblacion.chart.addDatasets($scope.chartPoblacion.datasets);
            $scope.chartPoblacion.chart.update();

            $scope.chartTropus.datasets[0].addDataAll($scope.chartTropusMachos);
            $scope.chartTropus.datasets[1].addDataAll($scope.chartTropusHembras);
            $scope.chartTropus.chart.addlabels($scope.chartAnios);
            $scope.chartTropus.chart.addDatasets($scope.chartTropus.datasets);
            $scope.chartTropus.chart.update();

            $scope.chartInopios.datasets[0].addDataAll($scope.chartInopiosMachos);
            $scope.chartInopios.datasets[1].addDataAll($scope.chartInopiosHembras);
            $scope.chartInopios.chart.addlabels($scope.chartAnios);
            $scope.chartInopios.chart.addDatasets($scope.chartInopios.datasets);
            $scope.chartInopios.chart.update();

            $scope.chartAliFifi.datasets[0].addDataAll($scope.chartTotalAlicanolas);
            $scope.chartAliFifi.datasets[1].addDataAll($scope.chartTotalFifirufas);
            $scope.chartAliFifi.chart.addlabels($scope.chartAnios);
            $scope.chartAliFifi.chart.addDatasets($scope.chartAliFifi.datasets);
            $scope.chartAliFifi.chart.update();
        };
    }
]);

$.when($.ready).then(function(){
    imgToSvg();
});
class Alicanola{
    constructor(posicion){
        this.tamanio = alicanolaConf.tamanioMin;
        this.posicion = posicion;
    };

    aumentarTamanio(){
        if(this.tamanio < (alicanolaConf.tamanioMin * 3))
            this.tamanio++;
    };
}
class MyChart{
    constructor(canvas, titulo){
        this.titulo = titulo;
        this.labels = [];
        this.datasets = [];

        this.chart = null;

        this.crear(canvas);
    }

    datasetsToJson(){
        let datasets = [];
        $.each(this.datasets, function(index, dataset){
            datasets.push(dataset.getJson())
        });
        return datasets;
    }

    getLineChartData(){
        return {
            labels: this.labels,
            datasets: this.datasetsToJson()
        };
    }

    crear(canvas){
        let ctx = document.getElementById(canvas).getContext("2d");
        this.chart = Chart.Line(ctx, {
            data: this.getLineChartData(),
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                title:{
                    fontColor: 'white',
                    display: true,
                    text: this.titulo
                },
                scales: {
                    yAxes: [{
                        type: "linear",
                        display: true,
                        position: "left",
                        id: "y-axis",
                        ticks: {
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white'
                        }
                    }]
                },
                legend: {
                    labels: { fontColor: 'white' }
                },
                elements: {
                    point: {
                        radius: 0
                    }
                }
            }
        });
    };

    addlabels(labels){
        this.labels = labels;
    }
    addDatasets(datasets){
        this.datasets = datasets;
    }

    update(){
        this.chart.data = this.getLineChartData();
        this.chart.update();
    }

    static chartColors() {
        return {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)',

            tropus: 'rgb(0, 128, 0)',
            inopius: 'rgb(173, 255, 47)'
        };
    };
}

class ChartDataSet{
    constructor(label, color){
        this.label = label;
        this.color = color;
        this.data = [];
    }

    addData(data){
        this.data.push(data);
    }
    addDataAll(data){
        this.data = data;
    }

    getJson(){
        return {
            label: this.label,
            borderColor: this.color,
            backgroundColor: this.color,
            fill: false,
            data: this.data
        };
    }
}
const tamanioMin = 12;
const timepoMax = 5000;
const velocidadMax = 5;

//PISPIRISPIS
const pispirispiConf = {
    tamanioMin: tamanioMin,
    velocidadMax: velocidadMax,

    generos: {
        m: 'Macho',
        h: 'Hembra'
    }
};

//ALICANOLAS
const alicanolaConf = {
    tamanioMin: tamanioMin*0.5
};
class Etapa{
    constructor(nombre, duracion, energia){
        this.nombre = nombre;
        this.duracion = duracion;
        this.energia = energia;
    }
}
class Fifirufas{
    constructor(posicion, tamanio){
        this.tamanio = tamanio;
        this.posicion = posicion;
    };
}
let funcionalidades = {
    posicionAzar: function(limites){
        let x = Math.round(Math.random() * (limites.ancho - pispirispiConf.tamanioMin));
        let y = Math.round(Math.random() * (limites.alto - pispirispiConf.tamanioMin));
        return new Punto(x, y);
    }
};

let sleep = function(ms){
    return new Promise(resolve => setTimeout(resolve, ms));
};

let random = function(a, b) {
    return Math.random()*(b-a)+parseInt(a);
};

let duplicateObject = function(object){
    return JSON.parse(JSON.stringify(object));
};

let imgToSvg = function(){
    $('.to-svg:not(.to-svg-check)').each(function(index) {
        let $id = 'svgCode__'+index;
        let $img = $(this);

        $img.attr('id',$id);
        $img.addClass('to-svg-check');

        let imgID = $img.attr('id');
        let imgClass = $img.attr('class');
        let imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            let $svg = jQuery(data).find('svg');
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);
        }, 'xml');
    });
};
class Pispirispi{
    constructor(tipo, genero, edad, etapas, posicion){
        this.tipo = tipo;
        this.genero = genero;
        this.edad = edad;
        this.posicion = posicion;

        this.etapas = etapas;
        this.etapa = {
            etapa: 0,
            duracion: 0,
            evolucionar: true
        };

        this.tamanio = pispirispiConf.tamanioMin;
        this.energia = 10000;

        this.fifirufa = null;

        this.direccion = { x:0, y:0 };
        this.setDireccion();

        this.reproduccion = false;
    }

    getPosicion(){
        return this.posicion;
    }
    setPosicion(x, y){
        this.posicion.setValues(x, y);
    }
    setDireccion(){
        let x = parseInt(random(-2, 2));
        let y = parseInt(random(-2, 2));
        while(x === 0 && y === 0){
            y = parseInt(random(-2, 2));
        }
        if(this.direccion.x === 0 || this.direccion.y === 0){
            while(x === 0)
                x = parseInt(random(-2, 2));
            while(y === 0)
                y = parseInt(random(-2, 2));
        }

        this.direccion = {
            x: x,
            y: y
        };
    }

    alistarFifirufa(tamanio){
        this.fifirufa = {
            tiempo: 10,
            tamanio: tamanio
        };
    }
    tiempoDefecar(){
        if(this.fifirufa !== null){
            if(this.fifirufa.tiempo <= 0)
                return true;
        }
        return false;
    }
    defecar(){
        this.fifirufa = null;
    }

    getVelocidad(direccion){
        const velMax = pispirispiConf.velocidadMax;
        let vel = velMax / (this.tamanio / pispirispiConf.tamanioMin);
        vel *= direccion;
        return vel;
    }

    movimiento(plano){
        let xMax = plano.ancho - this.tamanio;
        let yMax = plano.alto - this.tamanio;

        let newPosicionX = this.posicion.x + this.getVelocidad(this.direccion.x);
        let newPosicionY = this.posicion.y + this.getVelocidad(this.direccion.y);
        while((newPosicionX > xMax || newPosicionX < 0) || (newPosicionY > yMax || newPosicionY < 0)){
            this.setDireccion();
            newPosicionX = this.posicion.x + this.getVelocidad(this.direccion.x);
            newPosicionY = this.posicion.y + this.getVelocidad(this.direccion.y);
        }
        this.setPosicion(newPosicionX, newPosicionY);
    }
    aumentarEdad(){
        this.edad++;

        this.etapa.duracion++;
        if(this.etapa.duracion >= this.etapas[this.etapa.etapa].duracion){
            let etapa = {
                etapa: this.etapas.length - 1,
                duracion: 0,
                evolucionar: false
            };
            if(this.etapa.evolucionar)
                etapa.etapa = this.etapa.etapa + 1;

            this.etapa = etapa;
        }

        this.energia = this.energia - this.etapas[this.etapa.etapa].energia;
        if(this.fifirufa !== null){
            this.fifirufa.tiempo--;
        }

        this.tamanio = this.tamanio + (1/100);
    }

    colision(posicionObjeto, tamanioObjeto){
        let left = this.posicion.x;
        let right = this.posicion.x + this.tamanio;
        let top = this.posicion.y + this.tamanio;
        let bottom = this.posicion.y;

        let r_left = posicionObjeto.x;
        let r_right = posicionObjeto.x + tamanioObjeto;
        let r_top = posicionObjeto.y + tamanioObjeto;
        let r_bottom = posicionObjeto.y;

        return (right >= r_left && left <= r_right && top >= r_bottom && bottom <= r_top);
    }
}
class Plano{
    constructor(ancho, alto){
        this.ancho = ancho;
        this.alto = alto;

        this.alicanolas = [];
        this.fifirufas = [];
    }

    getDimensiones(){
        return {
            ancho: this.ancho,
            alto: this.alto
        }
    }

    crearAlicanola(){
        let alicanola = new Alicanola(funcionalidades.posicionAzar(this.getDimensiones()));
        this.alicanolas.push(alicanola);
        return alicanola;
    };
    crecerAlicanolas(){
        $.each(this.alicanolas, function(index, alicanola){
            alicanola.tamanio = alicanola.tamanio + (1/200);
        });
    };
    eliminarAlicanola(alicanolaOut){
        let tmpAlicanolas = [];
        $.each(this.alicanolas, function(index, alicanola){
            if(alicanola !== alicanolaOut)
                tmpAlicanolas.push(alicanola);
        });
        this.alicanolas = tmpAlicanolas;
    }

    crearFifirufa(posicion, tamanio){
        let fifirufa = new Fifirufas(posicion, tamanio);
        this.fifirufas.push(fifirufa);
        return fifirufa;
    }
    eliminarFifirufa(fifirufaOut){
        let tmpFifirufas = [];
        $.each(this.fifirufas, function(index, fifirufa){
            if(fifirufa !== fifirufaOut)
                tmpFifirufas.push(fifirufa);
        });
        this.fifirufas = tmpFifirufas;
    }
}
class Poblacion{
    constructor(nombre, machos, hembras, etapas, limites){
        this.individuos = [];

        this.nombre = nombre;

        this.machos = 0;
        this.hembras = 0;

        this.etapas = etapas;

        let i;
        for(i = 0; i < machos; i++){
            this.agregarIndividuo(
                pispirispiConf.generos.m,
                0,
                funcionalidades.posicionAzar(limites)
            );
        }
        for(i = 0; i < hembras; i++){
            this.agregarIndividuo(
                pispirispiConf.generos.h,
                0,
                funcionalidades.posicionAzar(limites)
            );
        }
    }

    agregarIndividuo(genero, edad, posicion){
        let pispirispi = new Pispirispi(this.nombre, genero, edad, this.etapas, posicion);
        this.individuos.push(pispirispi);
        if(pispirispi.genero === pispirispiConf.generos.m)
            this.machos++;
        else
            this.hembras++;
        return pispirispi;
    }
    matarIndividuo(pispirispiOut){
        let tmpIndividuos = [];
        let obj = this;
        $.each(this.individuos, function(index, pispirispi){
            if(pispirispi !== pispirispiOut)
                tmpIndividuos.push(pispirispi);
            else{
                if(pispirispi.genero === pispirispiConf.generos.m)
                    obj.machos--;
                else
                    obj.hembras--;
            }
        });
        this.individuos = tmpIndividuos;
    }
}
class Punto{
    constructor(x, y){
        this.x = x;
        this.y = y;
    }

    setValues(x, y){
        this.x = x;
        this.y = y;
    }
}