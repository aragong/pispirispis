const pispirispisApp = angular.module('pispirispis', []);

pispirispisApp.controller('main', [
    '$scope',
    '$q',
    function($scope, $q){
        let defered = $q.defer();
        let promise = defered.promise;

        $scope.tiempoTranscurrido = 0;

        promise.then(function(resultado) {
        }, function(error) {
        }, function() {
            $scope.tiempoTranscurrido++;

            if(($scope.tiempoTranscurrido % 2) === 1)
                $scope.crearPoblacionInicial();

            $scope.gestionarAlicanolas();
            $scope.recorrerPoblacion();

            if($scope.tropus.individuos.length === 0 && $scope.inopios.individuos.length === 0)
                $scope.avanzar = false;

            //GRAFICAR
            if(($scope.tiempoTranscurrido % 30) === 0) {
                $scope.chartAnios.push($scope.tiempoTranscurrido);

                $scope.chartTotalTropus.push($scope.tropus.individuos.length);
                $scope.chartTotalInopios.push($scope.inopios.individuos.length);

                $scope.chartTropusMachos.push($scope.tropus.machos);
                $scope.chartTropusHembras.push($scope.tropus.hembras);

                $scope.chartInopiosMachos.push($scope.inopios.machos);
                $scope.chartInopiosHembras.push($scope.inopios.hembras);

                $scope.chartTotalAlicanolas.push($scope.plano.alicanolas.length);
                $scope.chartTotalFifirufas.push($scope.plano.fifirufas.length);

                $scope.graficar();
            }
        });


        $scope.avanzar = false;

        $scope.etapasConfig = [
            new Etapa('Nacimiento', 0, 0),
            new Etapa('Infancia', (75*2), 1),
            new Etapa('Adolecencia', (75*3), 2),
            new Etapa('Adultez', (75*40), 3),
            new Etapa('Vejez', (75*3), 4),
            new Etapa('Muerte', 0, 0)
        ];

        $scope.planoConf = {
            ancho: 1040,
            alto: 650
        };
        $scope.generos = pispirispiConf.generos;
        $scope.poblacionInicial = {
            tropus: { machos: 30, hembras: 30 },
            inopios: { machos: 30, hembras: 30 }
        };

        $scope.alicanolasInicial = 30;
        $scope.generacionAlicanolas = {
            tiempo: 10,
            cantidadMax: 3
        };

        $scope.energiaFifirufa = 100;

        $scope.velocidad = 50;
        $scope.calcularVelocidad = function(){
            return timepoMax / $scope.velocidad;
        };

        $scope.mostrarDatos = false;


        $scope.crearPoblacionInicial = function(){
            const tipo = parseInt(random(0, 2));
            const genero = parseInt(random(0, 2));
            let poblacion = $scope.tropus;
            let poblacionConf = $scope.tmpPoblacionConf.tropus;
            if(tipo === 1){
                poblacion = $scope.inopios;
                poblacionConf = $scope.tmpPoblacionConf.inopios;
            }
            if((poblacionConf.machos + poblacionConf.hembras) > 0){
                let generoObj = null;
                if(genero === 1 && poblacionConf.machos > 0){
                    generoObj = pispirispiConf.generos.m;
                    poblacionConf.machos--;
                }
                else if(genero === 0 && poblacionConf.hembras > 0){
                    generoObj = pispirispiConf.generos.h;
                    poblacionConf.hembras--;
                }
                if(generoObj !== null) {
                    poblacion.agregarIndividuo(
                        generoObj,
                        0,
                        funcionalidades.posicionAzar($scope.plano.getDimensiones()));
                }
            }
        };

        $scope.recorrerPoblacion = function(){
            let pispirispisLista = [];

            function verificarColicion(pispirispi, poblacion){
                let poblacion1 = poblacion;
                let poblacion2 = (poblacion === $scope.tropus)?$scope.inopios:$scope.tropus;

                $.each($scope.plano.alicanolas, function(index, alicanola){
                    if(pispirispi.colision(alicanola.posicion, alicanola.tamanio)){
                        if(alicanola.tamanio <= (pispirispi.tamanio / 1.5)){
                            pispirispi.energia += alicanola.tamanio*0.75*100;
                            pispirispi.alistarFifirufa(alicanola.tamanio*0.25);
                            $scope.plano.eliminarAlicanola(alicanola);
                        }
                    }
                });

                $.each($scope.plano.fifirufas, function(index, fifirufa){
                    if(pispirispi.colision(fifirufa.posicion, fifirufa.tamanio)){
                        if(!(pispirispi.tipo === 'inopios' && pispirispi.etapa.etapa === 3))
                            pispirispi.energia = pispirispi.energia - $scope.energiaFifirufa;
                        else
                            $scope.plano.eliminarFifirufa(fifirufa);
                    }
                });

                $.each(pispirispisLista, function(index, value){
                    if(pispirispi.colision(value.getPosicion(), value.tamanio)){
                        if(pispirispi.tipo !== value.tipo){
                            if(pispirispi.genero === value.genero &&
                                (pispirispi.etapa.etapa === 2 && value.etapa.etapa === 2)){
                                if(pispirispi.energia < value.energia){
                                    value.etapa.evolucionar = true;
                                    poblacion1.matarIndividuo(pispirispi);
                                }
                                else if(pispirispi.energia > value.energia){
                                    pispirispi.etapa.evolucionar = true;
                                    poblacion2.matarIndividuo(value);
                                }
                            }
                            else if(pispirispi.edad < value.edad){
                                value.etapa.evolucionar = true;
                                poblacion1.matarIndividuo(pispirispi);
                            }
                            else if(pispirispi.edad > value.edad){
                                pispirispi.etapa.evolucionar = true;
                                poblacion2.matarIndividuo(value);
                            }
                        }
                        else{
                            if(pispirispi.genero !== value.genero){
                                if(pispirispi.etapa.etapa === 3 && value.etapa.etapa === 3){ //REPRODUCCION
                                    pispirispi.reproduccion = true;
                                }
                            }
                        }


                        pispirispi.setDireccion();
                        value.setDireccion();
                    }
                });
                pispirispisLista.push(pispirispi);
            }
            function necesitaDefecar(pispirispi){
                if(pispirispi.tiempoDefecar()){
                    $scope.plano.crearFifirufa(new Punto(pispirispi.getPosicion().x, pispirispi.getPosicion().y), pispirispi.fifirufa.tamanio);
                    pispirispi.defecar();
                }
            }
            function recorrerPoblacion(poblacion){
                $.each(poblacion.individuos, function(index, pispirispi){
                    pispirispi.aumentarEdad();
                    pispirispi.movimiento($scope.plano);

                    if(pispirispi.etapa.etapa === (pispirispi.etapas.length - 1) || pispirispi.energia <= 0)
                        poblacion.matarIndividuo(pispirispi);
                    else{
                        verificarColicion(pispirispi, poblacion);
                        necesitaDefecar(pispirispi);
                    }

                    if(pispirispi.reproduccion){
                        let colision = false;
                        $.each(poblacion.individuos, function(key, pareja){
                            if(pispirispi !== pareja){
                                if(pispirispi.colision(pareja.getPosicion(), pareja.tamanio)){
                                    colision = true;
                                }
                            }
                        });
                        if(!colision){
                            pispirispi.reproduccion = false;
                            let generoObj = (parseInt(random(0, 2)) === 1)?pispirispiConf.generos.m:pispirispiConf.generos.h;
                            poblacion.agregarIndividuo(
                                generoObj,
                                0,
                                new Punto(pispirispi.getPosicion().x, pispirispi.getPosicion().y));
                        }
                    }
                });
            }

            recorrerPoblacion($scope.tropus);
            recorrerPoblacion($scope.inopios);
        };
        $scope.gestionarAlicanolas = function(){
            //CREAR
            if(($scope.tiempoTranscurrido % $scope.generacionAlicanolas.tiempo) === 0){
                for(let i = 0; i < parseInt(random(-1, $scope.generacionAlicanolas.cantidadMax + 1)); i++){
                    $scope.plano.crearAlicanola();
                }
            }
            //AUMENTAR TAMAÑO
            $scope.plano.crecerAlicanolas();
        };

        //INIT
        $scope.iniciar = async function(){
            $scope.tiempoTranscurrido = 0;
            $scope.avanzar = true;

            $scope.plano = new Plano($scope.planoConf.ancho, $scope.planoConf.alto);

            $scope.tropus = new Poblacion('tropus',
                0,
                0,
                $scope.etapasConfig,
                $scope.plano.getDimensiones()
            );
            $scope.inopios = new Poblacion('inopios',
                0,
                0,
                $scope.etapasConfig,
                $scope.plano.getDimensiones()
            );
            $scope.tmpPoblacionConf = duplicateObject($scope.poblacionInicial);

            for(let i = 0; i < $scope.alicanolasInicial; i++){
                $scope.plano.crearAlicanola();
            }

            while($scope.avanzar){
                await sleep($scope.calcularVelocidad());
                defered.notify();
            }
        };

        $scope.submit = function(){
            if(!$scope.avanzar)
                $scope.iniciar();
            else
                $scope.avanzar = false;
        };


        $scope.toogleDatos = function(){
            $scope.mostrarDatos = !$scope.mostrarDatos;
        };

        // GRAFICAR DATOS
        $scope.chartAnios = []; //xAxis

        $scope.chartPoblacion = {
            chart: new MyChart('canvas-1','Población Total'),
            datasets: [
                new ChartDataSet('Tropus', MyChart.chartColors().tropus),
                new ChartDataSet('Inopios', MyChart.chartColors().inopius)
            ]
        };
        $scope.chartTotalTropus = [];
        $scope.chartTotalInopios = [];

        $scope.chartTropus = {
            chart: new MyChart('canvas-2','Población de Tropus'),
            datasets: [
                new ChartDataSet('Machos', MyChart.chartColors().blue),
                new ChartDataSet('Hembras', MyChart.chartColors().red)
            ]
        };
        $scope.chartTropusMachos = [];
        $scope.chartTropusHembras = [];

        $scope.chartInopios = {
            chart: new MyChart('canvas-3','Población de Inopios'),
            datasets: [
                new ChartDataSet('Machos', MyChart.chartColors().blue),
                new ChartDataSet('Hembras', MyChart.chartColors().red)
            ]
        };
        $scope.chartInopiosMachos = [];
        $scope.chartInopiosHembras = [];

        $scope.chartAliFifi = {
            chart: new MyChart('canvas-4','Alicanolas - Fifirufas'),
            datasets: [
                new ChartDataSet('Alicanolas', MyChart.chartColors().purple),
                new ChartDataSet('Fifirufas', MyChart.chartColors().orange)
            ]
        };
        $scope.chartTotalAlicanolas = [];
        $scope.chartTotalFifirufas = [];

        $scope.graficar = function(){
            $scope.chartPoblacion.datasets[0].addDataAll($scope.chartTotalTropus);
            $scope.chartPoblacion.datasets[1].addDataAll($scope.chartTotalInopios);
            $scope.chartPoblacion.chart.addlabels($scope.chartAnios);
            $scope.chartPoblacion.chart.addDatasets($scope.chartPoblacion.datasets);
            $scope.chartPoblacion.chart.update();

            $scope.chartTropus.datasets[0].addDataAll($scope.chartTropusMachos);
            $scope.chartTropus.datasets[1].addDataAll($scope.chartTropusHembras);
            $scope.chartTropus.chart.addlabels($scope.chartAnios);
            $scope.chartTropus.chart.addDatasets($scope.chartTropus.datasets);
            $scope.chartTropus.chart.update();

            $scope.chartInopios.datasets[0].addDataAll($scope.chartInopiosMachos);
            $scope.chartInopios.datasets[1].addDataAll($scope.chartInopiosHembras);
            $scope.chartInopios.chart.addlabels($scope.chartAnios);
            $scope.chartInopios.chart.addDatasets($scope.chartInopios.datasets);
            $scope.chartInopios.chart.update();

            $scope.chartAliFifi.datasets[0].addDataAll($scope.chartTotalAlicanolas);
            $scope.chartAliFifi.datasets[1].addDataAll($scope.chartTotalFifirufas);
            $scope.chartAliFifi.chart.addlabels($scope.chartAnios);
            $scope.chartAliFifi.chart.addDatasets($scope.chartAliFifi.datasets);
            $scope.chartAliFifi.chart.update();
        };
    }
]);

$.when($.ready).then(function(){
    imgToSvg();
});