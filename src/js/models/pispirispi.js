class Pispirispi{
    constructor(tipo, genero, edad, etapas, posicion){
        this.tipo = tipo;
        this.genero = genero;
        this.edad = edad;
        this.posicion = posicion;

        this.etapas = etapas;
        this.etapa = {
            etapa: 0,
            duracion: 0,
            evolucionar: true
        };

        this.tamanio = pispirispiConf.tamanioMin;
        this.energia = 10000;

        this.fifirufa = null;

        this.direccion = { x:0, y:0 };
        this.setDireccion();

        this.reproduccion = false;
    }

    getPosicion(){
        return this.posicion;
    }
    setPosicion(x, y){
        this.posicion.setValues(x, y);
    }
    setDireccion(){
        let x = parseInt(random(-2, 2));
        let y = parseInt(random(-2, 2));
        while(x === 0 && y === 0){
            y = parseInt(random(-2, 2));
        }
        if(this.direccion.x === 0 || this.direccion.y === 0){
            while(x === 0)
                x = parseInt(random(-2, 2));
            while(y === 0)
                y = parseInt(random(-2, 2));
        }

        this.direccion = {
            x: x,
            y: y
        };
    }

    alistarFifirufa(tamanio){
        this.fifirufa = {
            tiempo: 10,
            tamanio: tamanio
        };
    }
    tiempoDefecar(){
        if(this.fifirufa !== null){
            if(this.fifirufa.tiempo <= 0)
                return true;
        }
        return false;
    }
    defecar(){
        this.fifirufa = null;
    }

    getVelocidad(direccion){
        const velMax = pispirispiConf.velocidadMax;
        let vel = velMax / (this.tamanio / pispirispiConf.tamanioMin);
        vel *= direccion;
        return vel;
    }

    movimiento(plano){
        let xMax = plano.ancho - this.tamanio;
        let yMax = plano.alto - this.tamanio;

        let newPosicionX = this.posicion.x + this.getVelocidad(this.direccion.x);
        let newPosicionY = this.posicion.y + this.getVelocidad(this.direccion.y);
        while((newPosicionX > xMax || newPosicionX < 0) || (newPosicionY > yMax || newPosicionY < 0)){
            this.setDireccion();
            newPosicionX = this.posicion.x + this.getVelocidad(this.direccion.x);
            newPosicionY = this.posicion.y + this.getVelocidad(this.direccion.y);
        }
        this.setPosicion(newPosicionX, newPosicionY);
    }
    aumentarEdad(){
        this.edad++;

        this.etapa.duracion++;
        if(this.etapa.duracion >= this.etapas[this.etapa.etapa].duracion){
            let etapa = {
                etapa: this.etapas.length - 1,
                duracion: 0,
                evolucionar: false
            };
            if(this.etapa.evolucionar)
                etapa.etapa = this.etapa.etapa + 1;

            this.etapa = etapa;
        }

        this.energia = this.energia - this.etapas[this.etapa.etapa].energia;
        if(this.fifirufa !== null){
            this.fifirufa.tiempo--;
        }

        this.tamanio = this.tamanio + (1/100);
    }

    colision(posicionObjeto, tamanioObjeto){
        let left = this.posicion.x;
        let right = this.posicion.x + this.tamanio;
        let top = this.posicion.y + this.tamanio;
        let bottom = this.posicion.y;

        let r_left = posicionObjeto.x;
        let r_right = posicionObjeto.x + tamanioObjeto;
        let r_top = posicionObjeto.y + tamanioObjeto;
        let r_bottom = posicionObjeto.y;

        return (right >= r_left && left <= r_right && top >= r_bottom && bottom <= r_top);
    }
}