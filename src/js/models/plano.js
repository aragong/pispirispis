class Plano{
    constructor(ancho, alto){
        this.ancho = ancho;
        this.alto = alto;

        this.alicanolas = [];
        this.fifirufas = [];
    }

    getDimensiones(){
        return {
            ancho: this.ancho,
            alto: this.alto
        }
    }

    crearAlicanola(){
        let alicanola = new Alicanola(funcionalidades.posicionAzar(this.getDimensiones()));
        this.alicanolas.push(alicanola);
        return alicanola;
    };
    crecerAlicanolas(){
        $.each(this.alicanolas, function(index, alicanola){
            alicanola.tamanio = alicanola.tamanio + (1/200);
        });
    };
    eliminarAlicanola(alicanolaOut){
        let tmpAlicanolas = [];
        $.each(this.alicanolas, function(index, alicanola){
            if(alicanola !== alicanolaOut)
                tmpAlicanolas.push(alicanola);
        });
        this.alicanolas = tmpAlicanolas;
    }

    crearFifirufa(posicion, tamanio){
        let fifirufa = new Fifirufas(posicion, tamanio);
        this.fifirufas.push(fifirufa);
        return fifirufa;
    }
    eliminarFifirufa(fifirufaOut){
        let tmpFifirufas = [];
        $.each(this.fifirufas, function(index, fifirufa){
            if(fifirufa !== fifirufaOut)
                tmpFifirufas.push(fifirufa);
        });
        this.fifirufas = tmpFifirufas;
    }
}