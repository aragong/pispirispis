const tamanioMin = 12;
const timepoMax = 5000;
const velocidadMax = 5;

//PISPIRISPIS
const pispirispiConf = {
    tamanioMin: tamanioMin,
    velocidadMax: velocidadMax,

    generos: {
        m: 'Macho',
        h: 'Hembra'
    }
};

//ALICANOLAS
const alicanolaConf = {
    tamanioMin: tamanioMin*0.5
};