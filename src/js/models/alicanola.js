class Alicanola{
    constructor(posicion){
        this.tamanio = alicanolaConf.tamanioMin;
        this.posicion = posicion;
    };

    aumentarTamanio(){
        if(this.tamanio < (alicanolaConf.tamanioMin * 3))
            this.tamanio++;
    };
}