class Poblacion{
    constructor(nombre, machos, hembras, etapas, limites){
        this.individuos = [];

        this.nombre = nombre;

        this.machos = 0;
        this.hembras = 0;

        this.etapas = etapas;

        let i;
        for(i = 0; i < machos; i++){
            this.agregarIndividuo(
                pispirispiConf.generos.m,
                0,
                funcionalidades.posicionAzar(limites)
            );
        }
        for(i = 0; i < hembras; i++){
            this.agregarIndividuo(
                pispirispiConf.generos.h,
                0,
                funcionalidades.posicionAzar(limites)
            );
        }
    }

    agregarIndividuo(genero, edad, posicion){
        let pispirispi = new Pispirispi(this.nombre, genero, edad, this.etapas, posicion);
        this.individuos.push(pispirispi);
        if(pispirispi.genero === pispirispiConf.generos.m)
            this.machos++;
        else
            this.hembras++;
        return pispirispi;
    }
    matarIndividuo(pispirispiOut){
        let tmpIndividuos = [];
        let obj = this;
        $.each(this.individuos, function(index, pispirispi){
            if(pispirispi !== pispirispiOut)
                tmpIndividuos.push(pispirispi);
            else{
                if(pispirispi.genero === pispirispiConf.generos.m)
                    obj.machos--;
                else
                    obj.hembras--;
            }
        });
        this.individuos = tmpIndividuos;
    }
}