class MyChart{
    constructor(canvas, titulo){
        this.titulo = titulo;
        this.labels = [];
        this.datasets = [];

        this.chart = null;

        this.crear(canvas);
    }

    datasetsToJson(){
        let datasets = [];
        $.each(this.datasets, function(index, dataset){
            datasets.push(dataset.getJson())
        });
        return datasets;
    }

    getLineChartData(){
        return {
            labels: this.labels,
            datasets: this.datasetsToJson()
        };
    }

    crear(canvas){
        let ctx = document.getElementById(canvas).getContext("2d");
        this.chart = Chart.Line(ctx, {
            data: this.getLineChartData(),
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                title:{
                    fontColor: 'white',
                    display: true,
                    text: this.titulo
                },
                scales: {
                    yAxes: [{
                        type: "linear",
                        display: true,
                        position: "left",
                        id: "y-axis",
                        ticks: {
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white'
                        }
                    }]
                },
                legend: {
                    labels: { fontColor: 'white' }
                },
                elements: {
                    point: {
                        radius: 0
                    }
                }
            }
        });
    };

    addlabels(labels){
        this.labels = labels;
    }
    addDatasets(datasets){
        this.datasets = datasets;
    }

    update(){
        this.chart.data = this.getLineChartData();
        this.chart.update();
    }

    static chartColors() {
        return {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)',

            tropus: 'rgb(0, 128, 0)',
            inopius: 'rgb(173, 255, 47)'
        };
    };
}

class ChartDataSet{
    constructor(label, color){
        this.label = label;
        this.color = color;
        this.data = [];
    }

    addData(data){
        this.data.push(data);
    }
    addDataAll(data){
        this.data = data;
    }

    getJson(){
        return {
            label: this.label,
            borderColor: this.color,
            backgroundColor: this.color,
            fill: false,
            data: this.data
        };
    }
}