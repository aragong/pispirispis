let funcionalidades = {
    posicionAzar: function(limites){
        let x = Math.round(Math.random() * (limites.ancho - pispirispiConf.tamanioMin));
        let y = Math.round(Math.random() * (limites.alto - pispirispiConf.tamanioMin));
        return new Punto(x, y);
    }
};

let sleep = function(ms){
    return new Promise(resolve => setTimeout(resolve, ms));
};

let random = function(a, b) {
    return Math.random()*(b-a)+parseInt(a);
};

let duplicateObject = function(object){
    return JSON.parse(JSON.stringify(object));
};

let imgToSvg = function(){
    $('.to-svg:not(.to-svg-check)').each(function(index) {
        let $id = 'svgCode__'+index;
        let $img = $(this);

        $img.attr('id',$id);
        $img.addClass('to-svg-check');

        let imgID = $img.attr('id');
        let imgClass = $img.attr('class');
        let imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            let $svg = jQuery(data).find('svg');
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);
        }, 'xml');
    });
};